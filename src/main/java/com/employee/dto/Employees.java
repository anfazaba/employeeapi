package com.employee.dto;

import java.util.List;
import lombok.Data;

@Data
public class Employees {
    private List<Employee> employees;
}

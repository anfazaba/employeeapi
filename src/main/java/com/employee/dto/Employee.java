package com.employee.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class Employee {
    
    private Long id;
    private String name;
    private String contractTypeName;
    private Long roleId;
    private String roleName;
    private String roleDescription;
    private BigDecimal hourlySalary;
    private BigDecimal monthlySalary;
    private BigDecimal annualSalary;

    public void calculateAnnualSalary(){
        if(this.contractTypeName.equals("HourlySalaryEmployee")) {
            this.annualSalary = this.hourlySalary.multiply(BigDecimal.valueOf(120)).multiply(BigDecimal.valueOf(12));
        } else if(this.contractTypeName.equals("MonthlySalaryEmployee")) {
            this.annualSalary = this.monthlySalary.multiply(BigDecimal.valueOf(12));
        } else {
            this.annualSalary = BigDecimal.valueOf(0);
        }
    }

}
    

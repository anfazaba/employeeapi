package com.employee.web;

import com.employee.services.IEmployeeService;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
@Slf4j
public class IndexController {
    
    @Autowired
    private IEmployeeService employeeService;
    
    @GetMapping("/")
    public String start(String id, Model model){
        log.debug("ID: " + id);
        model.addAttribute("employees", employeeService.getEmployeeId(id));
        if(id==null){
            id = "";
        }
        model.addAttribute("id_serch", id);
        return "index";
    }
    
    
}


package com.employee.services.impl;

import com.employee.dto.Employee;
import com.employee.dto.Employees;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class RestClient {
    final String URL_EMPLOYEES = "http://masglobaltestapi.azurewebsites.net/api/Employees";
    
    private RestTemplate restTemplate = new RestTemplate();
    
    public List<Employee> getEmployees(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        
        ResponseEntity<List<Employee>> result = restTemplate.exchange(URL_EMPLOYEES, HttpMethod.GET, entity, new ParameterizedTypeReference<List<Employee>>(){});

        List<Employee> employees = result.getBody();
        for (Employee employee: employees ) {
            employee.calculateAnnualSalary();
        }

        return employees;
    }
}

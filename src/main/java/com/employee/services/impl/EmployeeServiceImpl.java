package com.employee.services.impl;

import com.employee.dto.Employee;
import com.employee.services.IEmployeeService;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
    
    private List<Employee> employees;
    
    private RestClient restClient = new RestClient();

    public EmployeeServiceImpl() {
        this.getUpdateEmployees();
    }

    private void getUpdateEmployees() {
        employees = restClient.getEmployees();
    }

    @Override
    public List<Employee> getEmployees() {
        return employees;
    }

    @Override
    public List<Employee> getEmployeeId(String id) {
        if(id == null || id == ""){
            return this.employees;
        }
        for (Employee employee: this.employees) {
            if(employee.getId().toString().equals(id)){
                List<Employee> employeeId = new ArrayList<>();
                employeeId.add(employee);
                return employeeId;
            }
        }
        return null;
    }
   
    
}

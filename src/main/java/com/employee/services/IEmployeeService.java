package com.employee.services;

import com.employee.dto.Employee;
import java.util.List;


public interface IEmployeeService {
    
   List<Employee> getEmployees();

   List<Employee> getEmployeeId(String id);

}
